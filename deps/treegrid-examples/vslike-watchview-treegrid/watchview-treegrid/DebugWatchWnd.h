//*******************************************************************************
// COPYRIGHT NOTES
// ---------------
// Copyright (C) 2014-2016 x-studio365.com.
// All rights reserved.
//
// This source code can be used, distributed or modified
// only under terms and conditions 
// of the accompanying license agreement.
//*******************************************************************************
//
#if !defined(AFX_DEBUGWATCHWND__INCLUDED_)
#define AFX_DEBUGWATCHWND__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// WatchBar.h : header file
//
#include "treegrid/TreeGridCtrl.h"

/////////////////////////////////////////////////////////////////////////////
// CWatchBar window

class CDebugWatchWnd : public CDockablePane
{
// Construction
public:
	CDebugWatchWnd();

	void EnableTreeGrid(BOOL bEnabled);
// Attributes
protected:
	CFont			m_Font;
	CTreeGridCtrl	m_wndWatchCtrl;
	CImageList      m_imageList;
// Operations
public:
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CWatchBar)
	//}}AFX_VIRTUAL

	virtual void GetPaneInfo(CString& strName, CString& strInfo, HICON& hIcon, BOOL& bAutoDestroyIcon)
	{
		//CBCGPDockingControlBar::GetPaneInfo(strName, strInfo, hIcon, bAutoDestroyIcon);
		strInfo = _T("Displays the Watch window to view the values of variables.");
#if 0
		if (hIcon == NULL)
		{
			// Current Visual theme doesn't have docking pane icons:
			int nImage = BCGPGetCmdMgr()->GetCmdImage(ID_VIEW_WATCH, FALSE);
			if (nImage >= 0)
			{
				hIcon = CBCGPToolBar::GetImages()->ExtractIcon(nImage);
				bAutoDestroyIcon = TRUE;
			}
		}
#endif
	}

// Implementation
public:
	virtual ~CDebugWatchWnd();

	// Generated message map functions
protected:
	//{{AFX_MSG(CWatchBar)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnSetFocus(CWnd* pOldWnd);

	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_WATCHBAR_H__31A1AF6B_7EB0_11D3_95C6_00A0C9289F1B__INCLUDED_)
