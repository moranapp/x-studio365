//*******************************************************************************
// COPYRIGHT NOTES
// ---------------
// Copyright (C) 2014-2016 x-studio365.com.
// All rights reserved.
//
// This source code can be used, distributed or modified
// only under terms and conditions 
// of the accompanying license agreement.
//*******************************************************************************
//
// DebugWatchWnd.cpp : implementation file
//
#include "stdafx.h"
#include "DebugWatchWnd.h"
//#include "skinsb.h"
#include "Resource.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CWatchBar

CDebugWatchWnd::CDebugWatchWnd()
{
}

CDebugWatchWnd::~CDebugWatchWnd()
{
}


BEGIN_MESSAGE_MAP(CDebugWatchWnd, CDockablePane)
	//{{AFX_MSG_MAP(CWatchBar)
	ON_WM_CREATE()
	ON_WM_SIZE()
	/*ON_WM_PAINT()*/
	ON_WM_SETFOCUS()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CWatchBar message handlers

int CDebugWatchWnd::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (__super::OnCreate(lpCreateStruct) == -1)
		return -1;

	// m_bVisualManagerStyle = TRUE;

	// LVN_COLUMNCLICK
	m_Font.CreateStockObject (SYSTEM_FONT);

	CRect rectDummy;
	rectDummy.SetRectEmpty ();

	const DWORD dwStyle = WS_CHILD | WS_VISIBLE;
	m_wndWatchCtrl.Create(rectDummy, this, 1, dwStyle);


#if 0
	// Setup ImageList
	m_imageList.Create(16, 15, ILC_COLOR24 | ILC_MASK, 3, 1);

	CBitmap cBmp;
	cBmp.LoadBitmap(IDB_CLASS_VIEW_HC);
	m_imageList.Add(&cBmp, RGB(255, 0, 0));
	cBmp.DeleteObject();
	int nImageCount = m_imageList.GetImageCount();
	m_wndWatchCtrl.SetImageList(&m_imageList);
#endif
	m_imageList.Create(MAKEINTRESOURCE(IDB_DEBUG_WATCH_HC), 16, 1, RGB(255, 255, 255));
	m_wndWatchCtrl.SetImageList(&m_imageList);

	// Initalize attributes of treegrid ctrl, look like Visual Studio 2015 dark style.
	m_wndWatchCtrl.SetFont(&m_Font, FALSE);
	
	m_wndWatchCtrl.SetGridBkColor(RGB(37, 37, 38));
	m_wndWatchCtrl.SetFixedBkColor(RGB(37, 37, 38)); // ONLY for test
													// m_wndWatchCtrl.SetFixedBkColor(RGB(42, 42, 44));
	m_wndWatchCtrl.SetGridLineColor(RGB(0, 0, 0));
	m_wndWatchCtrl.SetTextColor(RGB(241, 241, 241));
	m_wndWatchCtrl.SetFixedTextColor(RGB(241, 241, 241));
	m_wndWatchCtrl.GetDefaultCell(FALSE, FALSE)->SetBackClr(RGB(37, 37, 38));

	m_wndWatchCtrl.EnableDragAndDrop(TRUE);

	m_wndWatchCtrl.SetFixedColumnCount(1);
	m_wndWatchCtrl.SetFixedRowCount(1);

	m_wndWatchCtrl.SetEditable(TRUE);
	m_wndWatchCtrl.SetHeaderSort(FALSE);
	m_wndWatchCtrl.SetListMode(TRUE);

	m_wndWatchCtrl.SetTrackFocusCell(TRUE);
	m_wndWatchCtrl.SetFrameFocusCell(TRUE);

	m_wndWatchCtrl.SetRowResize(FALSE);
	m_wndWatchCtrl.SetColumnResize(TRUE);

	m_wndWatchCtrl.SetFixedColumnSelection(FALSE);
	m_wndWatchCtrl.SetFixedRowSelection(TRUE);
	m_wndWatchCtrl.EnableColumnHide();

	// Initialize columns
	m_wndWatchCtrl.InsertColumn(L"Name");
	m_wndWatchCtrl.InsertColumn(L"Value");
	m_wndWatchCtrl.InsertColumn(L"Type");

	m_wndWatchCtrl.SetColumnWidth(0, 10);


	// Inserts a example row.
	auto identeifiers = m_wndWatchCtrl.InsertItem(L"identifiers"); // Inserts a example row
	m_wndWatchCtrl.SetItemState(1, 2, GVIS_READONLY); // Value is readonly for example row
	m_wndWatchCtrl.SetItemState(1, 3, GVIS_READONLY); // Type is readonly always 
	m_wndWatchCtrl.SetItemText(1, 3, L"table");

	m_wndWatchCtrl.InsertItem(L"[1]", identeifiers);
	m_wndWatchCtrl.SetItemImage(2, 1, 5);
	m_wndWatchCtrl.SetItemState(2, 2, GVIS_READONLY);
	m_wndWatchCtrl.SetItemState(2, 3, GVIS_READONLY);

	m_wndWatchCtrl.SetItemText(2, 2, L"288");
	m_wndWatchCtrl.SetItemText(2, 3, L"int");

	// Inserts the new empty row.
	auto placehoder = m_wndWatchCtrl.InsertItem(L""); // Inserts a emtpy row
	m_wndWatchCtrl.SetItemState(3, 2, GVIS_READONLY); // Value is readonly for empty row
	m_wndWatchCtrl.SetItemState(3, 3, GVIS_READONLY); // Type is readonly always 
	

	int identifiers[] = { 288, 399, 400 };
	return 0;
}

void CDebugWatchWnd::EnableTreeGrid(BOOL bEnabled)
{ // ONLY dark style.
	if (bEnabled) {
		m_wndWatchCtrl.SetGridBkColor(RGB(37, 37, 38));
		m_wndWatchCtrl.SetFixedBkColor(RGB(37, 37, 38)); //  m_wndWatchCtrl.SetFixedBkColor(RGB(42, 42, 44));
		m_wndWatchCtrl.SetGridLineColor(RGB(0, 0, 0));
		m_wndWatchCtrl.SetTextColor(RGB(241, 241, 241));
		m_wndWatchCtrl.SetFixedTextColor(RGB(241, 241, 241));
		m_wndWatchCtrl.GetDefaultCell(FALSE, FALSE)->SetBackClr(RGB(37, 37, 38));
	}
	else {
		m_wndWatchCtrl.SetGridBkColor(RGB(42, 42, 44));
		m_wndWatchCtrl.SetFixedBkColor(RGB(42, 42, 44)); // m_wndWatchCtrl.SetFixedBkColor(RGB(42, 42, 44));
		m_wndWatchCtrl.SetGridLineColor(RGB(28, 28, 30));
		m_wndWatchCtrl.SetTextColor(RGB(119, 119, 121));
		m_wndWatchCtrl.SetFixedTextColor(RGB(119, 119, 121));
		m_wndWatchCtrl.GetDefaultCell(FALSE, FALSE)->SetBackClr(RGB(42, 42, 44));
	}

	m_wndWatchCtrl.EnableWindow(bEnabled);
}

void CDebugWatchWnd::OnSize(UINT nType, int cx, int cy) 
{
	__super::OnSize(nType, cx, cy);

	/*if (CanAdjustLayout ())
	{*/
		CRect rc;
		GetClientRect(rc);

		int avialWidth = (rc.Width() - 10);
		m_wndWatchCtrl.SetColumnWidth(1, avialWidth * 0.4);
		m_wndWatchCtrl.SetColumnWidth(2, avialWidth * 0.4);
		m_wndWatchCtrl.SetColumnWidth(3, avialWidth * 0.2);

		m_wndWatchCtrl.SetWindowPos(NULL,
				rc.left + 1, rc.top + 1,
				rc.Width() - 2, rc.Height () - 2,
				SWP_NOACTIVATE | SWP_NOZORDER);

		m_wndWatchCtrl.RedrawWindow();
	/*}*/
}


void CDebugWatchWnd::OnSetFocus(CWnd* pOldWnd)
{
	__super::OnSetFocus(pOldWnd);
	
#if 1
	m_wndWatchCtrl.SetFocus ();
#endif
}
