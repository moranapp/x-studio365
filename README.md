#x-studio365下载安装说明
**请直接在官网: http://cn.x-studio365.com 点击【立即下载】按钮下载安装本编辑器**

#x-studio365项目介绍

    x-studio365是一款基于Cocos2d-x游戏引擎开发的工具；具有的完整2D粒子编辑功能，场景（UI）编辑功能，屏幕适配预览功能（让手动适配问题彻底解放）。

      cocos2d-x引擎版本3.15, spine运行库版本3.5, DragonBones运行库5.0

      *** 注意如果安装过程中提示重启电脑, 选取消即可 ***

      编辑器主要特色：

* 直接使用Cocos2d-x渲染主编辑界面

* 支持多达20种布局对齐方式

* 支持任意有效Windows路径(包含中文,空格), 自动将中文图片文件翻译为拼音,将空格替换为下划线,避免发布到.csd或.csb后,引擎不支持中文的困扰

* 节点多选，整体移动，改变大小

* 画布缩放，浮动

* 支持粒子编辑功能

* 屏幕适配预览功能,可预览任意设备尺寸效果

* 支持导入CocosStudio ccs工程及反导入CocosStudio发布的json和csb格式ui, 【文件】【导入】【CocosStudio(*.ccs/*.json/*.csb)】，需先新建一个空工程

* 支持导入csb(将csb文件拖入编辑器场景即可)和发布到csb给ccoos2d-x引擎直接使用

* 灵活的UI节点父子关系，不论是精灵，按钮，层还是粒子节点均可作为父节点承载其他节点

* 集成了AES批量数据加密工具, 具有AES CBC模式批量数据加密解密，MD5校验，特定格式文件提取，差异文件提取等功能.

* spine骨骼动画预览用法：将spine导出的.atlas,.json/.skel,.png三者之一拖入场景即可,支持动画切换,事件查看,速度调整,骨骼缩放,着色器选择

* 粒子编辑器，支持动态范围滑块调参

* 支持发布为CocosStudio工程

* 支持发布任意节点为CocosStudio工程或.csb文件

* 属性编辑器支持拖入文件

* 场景编辑器支持拖入多张图片

* 场景对象探查器支持方向键微调节点位置

* 支持按住Shift键固定水平和垂直移动节点

* 支持多语言: 简体中文和英文

* 支持按钮标题文本特效,弥补CocosStudio不支持的缺憾

* 支持spritesheet合图创建

* 2D粒子编辑功能已完全支持，可编辑和导入导出粒子PLIST

#结合Cocos2d-x引擎使用说明
为了能够使用本编辑器相对于最新版本CocosStudio新增控件，例如ControlSwitch, RadioButton, Armature, SpineSkeleton, TiledMap
针对native或者Lua项目，请下载本编辑器专用引擎(只改写了cocos reader加载库，其他将于官方保持同步更新)

**目前有一个demo工程在 http://git.oschina.net/halx99/cocos2d-x/tree/v3/tests/x-studio365-projects/ 目录下**

步骤(国内站点，下载速度杠杠的):
 1.如果没有git则先安装git
 2.使用命令:
   下载引擎: git clone https://git.oschina.net/halx99/cocos2d-x.git
            引擎下载完成后，先在引擎根目录执行git submodule update --init --recursive将引擎tools等子模块下载下来
   下载第三方依赖库: git clone https://git.oschina.net/halx99/cocos2d-x-3rd-party-libs-bin.git
 3.手动将第三方所有依赖库拷贝到引擎external目录下
 4.打开cocos2d-win32.sln, 编译运行cpp-empty-test项目即可看到全部支持控件效果:
 ![输入图片说明](https://git.oschina.net/uploads/images/2017/0709/170038_e4c6f303_960647.png "在这里输入图片标题")
 
另外使用本编辑完界面需确保当前CSLoader版本为 10.0.2200.0 方可发布新控件到csb.
 ![输入图片说明](https://git.oschina.net/uploads/images/2017/0709/170056_cbac4b7a_960647.png "在这里输入图片标题")
#x-studio365项目官网: 
  http://x-studio365.com
  http://en-us.x-studio365.com    
